﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

			string login = "cant";
			string pass = "333";
			System.Console.WriteLine($"1. Вывод информации о заданном аккаунте по логину и паролю; Login = {login}, Password = {pass}");
			atmManager.PrintUserInfo(login, pass);
			System.Console.WriteLine();
			System.Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя;");
			atmManager.PrintUserAccountsInfo(login, pass);
			System.Console.WriteLine();
			System.Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;");
			atmManager.PrintUserAccountsHistoryInfo(login, pass);
			System.Console.WriteLine();
			System.Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;");
			atmManager.PrinAllUserHistoryInfo();
			System.Console.WriteLine();
			System.Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);");
			atmManager.PrintUsersWithMoneyGreaterThen(6000);

			System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}