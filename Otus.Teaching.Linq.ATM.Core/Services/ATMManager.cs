﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

		public void PrintUserInfo(string login, string password)
		{
			var user = GetUser(login, password);
			PrintUserInfo(user);
		}

		public void PrintUserAccountsInfo(string login, string password)
		{
			var user = GetUser(login, password);
			foreach (var account in GetAccounts(user))
			{
				PrintAccountInfo(account);
			}
		}

		public void PrintUserAccountsHistoryInfo(string login, string password)
		{
			var user = GetUser(login, password);
			foreach (var account in GetAccounts(user))
			{
				PrintAccountInfo(account);
				Console.WriteLine("History:");
				var histories = GetOperationsHistories(account);
				foreach (var history in histories)
				{
					PrintHistoryInfo(history);
				}
				PrintLine();
			}
		}

		public void PrinAllUserHistoryInfo()
		{
			var query =
				from h in History
				join a in Accounts on h.AccountId equals a.Id
				join u in Users on a.UserId equals u.Id
				group h by u into g
				select g;
			foreach (var group in query)
			{
				PrintUserInfo(group.Key);
				foreach (var groupItem in group)
				{
					PrintHistoryInfo(groupItem);
				}
				PrintLine();
			}
		}

		public void PrintUsersWithMoneyGreaterThen(decimal value)
		{
			var users = Accounts.Where(a => a.CashAll > value)
				.Select(a => Users.FirstOrDefault(u => u.Id == a.UserId))
				.Where(u => u != null)
				.Distinct();
			foreach (var user in users)
			{
				PrintUserInfo(user);
				PrintLine();
			}
		}

		void PrintLine()
		{
			Console.WriteLine("-------------------------------------------------------------");
		}

		void PrintUserInfo(User user)
		{
			Console.WriteLine($"{user.FirstName} {user.MiddleName} {user.SurName}");
			Console.WriteLine($"Registered: {user.RegistrationDate}");
			Console.WriteLine($"Passport: {user.PassportSeriesAndNumber}");
			Console.WriteLine($"Phone: {user.Phone}");
			Console.WriteLine($"Login: \"{user.Login}\" Password: \"{user.Password}\"");
		}

		void PrintAccountInfo(Account account)
		{
			Console.WriteLine($"Opened on {account.OpeningDate}. Bill amount: {account.CashAll}");
		}

		void PrintHistoryInfo(OperationsHistory history)
		{
			Console.WriteLine($"{(history.OperationType == OperationType.InputCash ? "Income" : "Out")}: {history.CashSum} on {history.OperationDate}");
		}

		User GetUser(string login, string password)
		{
			return Users.FirstOrDefault(u => u.Login == login && u.Password == password);
		}

		IEnumerable<Account> GetAccounts(User user)
		{
			return Accounts.Where(a => a.UserId == user.Id).ToList();
		}

		IEnumerable<OperationsHistory> GetOperationsHistories(Account account)
		{
			return History.Where(h => h.AccountId == account.Id).ToList();
		}
    }
}